using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    public Vector2 direction;
    public Rigidbody2D body;

    [SerializeField] private InputController input = null;
    [SerializeField] private Transform target;
    [SerializeField] private float lookAheadDistance;
    [SerializeField] private float cameraSpeed;
    [SerializeField] private float localscale;
    [SerializeField] private float cameraheight;
    public GameObject[] cameras;

    private int camerapos;
    private float lookAhead;
    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }
    private void OnLevelWasLoaded(int level)
    {
        cameras = GameObject.FindGameObjectsWithTag("MainCamera");
        if (cameras.Length > 1)
        {
            Destroy(cameras[1]);
        }
    }
    void Awake()
    {
        body = GetComponent<Rigidbody2D>();
    }
    // Update is called once per frame
    private void Update()
    {      
        direction.x = input.RetriveMoveInput();
        if (direction.x > 0)
            camerapos = 1;
        if (direction.x < 0)
            camerapos = -1;
        else
            camerapos = 0;

        transform.position = new Vector3(target.position.x + lookAhead, target.transform.position.y + cameraheight, transform.position.z);
        lookAhead = Mathf.Lerp(lookAhead, (lookAheadDistance * camerapos), Time.deltaTime * cameraSpeed);
    }
}
