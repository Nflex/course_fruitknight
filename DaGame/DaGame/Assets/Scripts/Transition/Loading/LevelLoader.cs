using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelLoader : MonoBehaviour
{
    [SerializeField] private LevelType levelType;
    private EndMenu endMenu;
    private void Awake()
    {
        endMenu = FindObjectOfType<EndMenu>();
    }
    public enum LevelType
    {
        Level1,
        Level2,
        TestLevel,
        MainMenu,
        LevelEnd,
        EndScene,
        EntryLevel
    }
    public LevelType GetLevelType()
    {
        return levelType;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        levelType = GetLevelType();

        if (collision.CompareTag("Player"))
        {
            switch (levelType)
            {
                default:
                case LevelType.TestLevel: Loader.Load(Loader.Scene.TestLevel); break;
                case LevelType.MainMenu: Loader.Load(Loader.Scene.MainMenu); break;
                case LevelType.Level1: Loader.Load(Loader.Scene.Level1); break;
                case LevelType.Level2: Loader.Load(Loader.Scene.Level2); break;
                case LevelType.EntryLevel: Loader.Load(Loader.Scene.EntryLevel); break;
                case LevelType.EndScene:

                    Loader.Load(Loader.Scene.EndScene);
                    Destroy(GameObject.FindWithTag("Player"));
                    Destroy(GameObject.FindWithTag("MainCamera"));
                    Destroy(GameObject.FindWithTag("UI"));
                    break;
            }
        }
    }
}
