using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class Loader{

    private class LoadingMonoBehaviour : MonoBehaviour { }
    public enum Scene{
        TestLevel,
        MainMenu,
        Loading,
        Level1,
        Level2,
        LevelEnd,
        EntryLevel,
        EndScene

    }

    private static Action onLoadingWait;
    private static AsyncOperation loadingAsyncOperation;

    public static void Load(Scene scene)
    {
        //Load target scene
        onLoadingWait = () =>{
            GameObject loadinggameObject = new GameObject("Loading game object");
            loadinggameObject.AddComponent<LoadingMonoBehaviour>().StartCoroutine(LoadSceneAsync(scene));
        };
        // Call Loading scene
        SceneManager.LoadScene(Scene.Loading.ToString());
    }
    private static IEnumerator LoadSceneAsync(Scene scene){
        yield return null;

        loadingAsyncOperation =  SceneManager.LoadSceneAsync(scene.ToString());

        while (!loadingAsyncOperation.isDone){
            yield return null;
        }
    }

    public static float GetLoadingBarProgress(){
        if(loadingAsyncOperation!= null){
            return loadingAsyncOperation.progress;
        }
        else{
            return 1f;
        }
    }

    public static void LoadingWait()
    {
        //Let's the screen to refresh
        //Loader call action that load's target scene
        if (onLoadingWait != null)
        {
            onLoadingWait();
            onLoadingWait = null;
        }
    }
}
