using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISave : MonoBehaviour
{
    public GameObject[] UI;
    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }
    private void OnLevelWasLoaded(int level)
    {
        UI = GameObject.FindGameObjectsWithTag("UI");
        if (UI.Length > 1)
        {
            Destroy(UI[1]);
        }
    }
}
