using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseMenu;
    private HealthSystem healthSystem;
    private Stash stash;
    private JumpUp jumpUp;
    public bool isPaused;

    // Start is called before the first frame update
    void Start()
    {
        pauseMenu.SetActive(false);
        healthSystem = FindObjectOfType<HealthSystem>();
        stash = FindObjectOfType<Stash>();
        jumpUp = FindObjectOfType<JumpUp>();
    }

    // Update is called once per frame
    void Update()
    {
      if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused){
                ResumeGame();
            }else{
                PauseGame();
            }
        }
    }

    public void PauseGame()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0;
        isPaused = true;
    }
    public void ResumeGame()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
    }
    public void RestartGame()
    {
        Destroy(GameObject.FindWithTag("Player"));
        Destroy(GameObject.FindWithTag("MainCamera"));
        Destroy(GameObject.FindWithTag("UI"));
        stash.UpdateBerrys();
        healthSystem.dead = false;
        Time.timeScale = 1f;
        healthSystem.AddHealth(healthSystem.basicHealth);
        Loader.Load(Loader.Scene.EntryLevel);
        GameObject.FindWithTag("GameOver").SetActive(false);
    }
    public void GotoMainMenu()
    {
        stash.UpdateBerrys();
        Destroy(GameObject.FindWithTag("Player"));
        Destroy(GameObject.FindWithTag("MainCamera"));
        Destroy(GameObject.FindWithTag("UI"));
        Time.timeScale = 1f;
        Loader.Load(Loader.Scene.MainMenu);
        isPaused = false;
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
