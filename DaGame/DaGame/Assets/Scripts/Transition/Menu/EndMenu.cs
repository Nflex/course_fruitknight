using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EndMenu : MonoBehaviour
{
    private Stash stash;

    public TextMeshProUGUI berryCountTotal;
    public TextMeshProUGUI orangeCountTotal;

    public TextMeshProUGUI eggCongrats;
    public TextMeshProUGUI eggBruh;
    public TextMeshProUGUI eggCountTotal;
    public TextMeshProUGUI eggCongratsAll;

    public Image eggImage;
    public Image eggImageAll;


    private int berrysTotal = 0;
    private int orangesTotal = 0;
    private int eggTotal = 0;

    private void Start()
    {
        stash = FindObjectOfType<Stash>();

        eggImageAll.enabled = false;
        eggCongratsAll.enabled = false;
        eggBruh.enabled = false;
        eggCongrats.enabled = false;
        eggCountTotal.enabled = false;
        eggImage.enabled = false;
        EndGuiUpdate();
    }
    public void EndGuiUpdate()
    {


        orangesTotal = Stash.oranges;
        berrysTotal = Stash.berrys;
        eggTotal = Stash.eggs;
        if (eggTotal > 0)
        {
            EggEnable();
        }
        if (eggTotal >= 4)
        {
            eggImageAll.enabled = true;
            eggCongratsAll.enabled = true;
            EggDisable();
        }else if(eggTotal <= 0)
        {
            eggBruh.enabled = true;
        }
        berryCountTotal.text = berrysTotal.ToString();
        orangeCountTotal.text = orangesTotal.ToString();
    }

    public void OnMainMenu()
    {
        Stash.berrys = 0;
        Stash.oranges = 0;
        Stash.eggs = 0;
        Time.timeScale = 1f;
        Loader.Load(Loader.Scene.MainMenu);
    }
    public void EggEnable()
    {
        eggImage.enabled = true;
        eggCountTotal.enabled = true;
        eggCongrats.enabled = true;
        eggCountTotal.text = eggTotal.ToString();
    }
    public void EggDisable()
    {
        eggImage.enabled = false;
        eggCountTotal.enabled = false;
        eggCongrats.enabled = false;
        eggCountTotal.enabled = false;
    }
}
