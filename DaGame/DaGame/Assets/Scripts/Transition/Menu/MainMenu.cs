using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    public void OnNewGameClicked()
    {
        Time.timeScale = 1f;
        Loader.Load(Loader.Scene.EntryLevel);
    }
    public void OnContinueGameClicked()
    {
        Application.Quit();
    }
}
