using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InputController : ScriptableObject
{
    public abstract float RetriveMoveInput();
    public abstract float RetriveVerticalInput();
    public abstract bool RetriveJumpInput();
    public abstract bool RetriveJumpInputUp();
}
