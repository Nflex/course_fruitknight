using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthSystem : MonoBehaviour
{
    [Header ("Health")]
    [SerializeField] public int basicHealth;
    public float currentHealth { get; private set; }
    private string currentAnimaton;
    public bool dead;
    private UIGameOver uiGameover;

    [Header("IFrames")]
    [SerializeField] private float iframeDuration;
    [SerializeField] private int flashNumber;
    private SpriteRenderer spriteRend;

    [Header("Components")]
    [SerializeField] private Behaviour[] components;

    public Animator animator;

    public const string PLAYER_HURT = "HurtAnimation";
    public const string PLAYER_DEATH = "DeathAnimation";
    public const string PLAYER_DEAD = "DeadAnimation";

    // Start is called before the first frame update
    private void Awake()
    {
        currentHealth = basicHealth;
        spriteRend = GetComponent<SpriteRenderer>();
        uiGameover = FindObjectOfType<UIGameOver>();
    }
    public void DamageTaken(float damage)
    {   
        currentHealth = Mathf.Clamp(currentHealth - damage, 0, basicHealth);
        if (currentHealth > 0)
        {
            ChangeAnimationState(PLAYER_HURT);
            StartCoroutine(playerHurtIdle());
            StartCoroutine(Invunerable());
            //hurt i frames
        }
        else
        {
            if (!dead)
            {
                foreach (Behaviour component in components)
                    component.enabled = false;
          
                ChangeAnimationState(PLAYER_DEATH);
                StartCoroutine(playerDead());
                dead = true;
                StartCoroutine(gameOverScreen());
            }
        }
    }
    public void AddHealth(float hpValue)
    {
        currentHealth = Mathf.Clamp(currentHealth + hpValue, 0, basicHealth);
    }
    private void Update()
    {
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.E))
        {
            DamageTaken(1);
        }
    }


    // mini animation manager
    void ChangeAnimationState(string newAnimation)
    {
        if (currentAnimaton == newAnimation) return;

        animator.Play(newAnimation);
        currentAnimaton = newAnimation;
    }
    private IEnumerator gameOverScreen()
    {
        yield return new WaitForSeconds((float)0.55);
        uiGameover.GameOver();
    }
    private IEnumerator playerDead()
    {
        yield return new WaitForSeconds((float)0.55);
        ChangeAnimationState(PLAYER_DEAD);
    }
    private IEnumerator playerHurtIdle()
    {
        yield return new WaitForSeconds((float)0.15);
        ChangeAnimationState(AnimationManager.PLAYER_IDLE);
    }
    private IEnumerator Invunerable()
    {
        Physics2D.IgnoreLayerCollision(3, 9, true);
        for (int i = 0; i < flashNumber; i++)
        {
            spriteRend.enabled = true;
            yield return new WaitForSeconds(iframeDuration / (flashNumber * 2));
            spriteRend.enabled = false;
            yield return new WaitForSeconds(iframeDuration / (flashNumber * 2));
        }
        Physics2D.IgnoreLayerCollision(3, 9, false);
    }
}
