using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_KeyHolder : MonoBehaviour
{
    [SerializeField] private Stash stash;
    private Transform Container;
    private Transform KeyTemplate;

    private void Awake()
    {
        Container = transform.Find("Container");
        KeyTemplate = Container.Find("KeyTemplate");
        KeyTemplate.gameObject.SetActive(false);
    }

    private void Start()
    {
        stash.OnKeysChanged += Stash_OnKeysChanged;
    }

    private void Stash_OnKeysChanged(object sender, System.EventArgs e)
    {
        UpdateVisual();
    }

    private void UpdateVisual()
    {
        //clean up
        foreach (Transform child in Container)
        {
            if (child == KeyTemplate) continue;
            Destroy(child.gameObject);
            if(KeyTemplate.gameObject == isActiveAndEnabled) KeyTemplate.gameObject.SetActive(false);
        }

        //container cycle
        List<Key.KeyType> keyList = stash.GetKeyList();
        for (int i = 0; i < keyList.Count; i++)
        {
            Key.KeyType keyType = keyList[i];
            Transform keyTransform = Instantiate(KeyTemplate, Container);
            KeyTemplate.gameObject.SetActive(true);
            keyTransform.GetComponent<RectTransform>().anchoredPosition = new Vector2(50 * i, 0);
            Image keyImage = keyTransform.Find("Image").GetComponent<Image>();
            switch (keyType)
            {
                default:
                case Key.KeyType.Silver: keyImage.color = Color.cyan;    break;
                case Key.KeyType.Bronze: keyImage.color = Color.grey;  break;
                case Key.KeyType.Gold: keyImage.color = Color.yellow;   break;
            }
        }
    }
}
