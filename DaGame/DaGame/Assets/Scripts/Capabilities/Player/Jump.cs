using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    [SerializeField] private InputController input = null;
    [SerializeField, Range(0f, 10f)] private float jumpHeight = 3f;
    [SerializeField, Range(0, 5)] private int maxAirJumps = 0;
    [SerializeField, Range(0f, 5f)] private float downwardMovementMultiplier = 3f;
    [SerializeField, Range(0f, 5f)] private float upwardMovementMultiplier = 1.7f;

    public Rigidbody2D body;
    private Ground ground;
    public Vector2 velocity;
    public Vector2 direction;

    private float coyoteTime = 0.2f;
    private float coyoteTimeCounter;
    private float jumpBufferTime = 0.1f;
    private float jumpBufferCounter;

    private int jumpPhase;
    private float defaultGravityScale;

    private bool desiredJump;
    private bool desiredJumpUp;
    private bool onGround;


    // Start is called before the first frame update
    void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        ground = GetComponent<Ground>();

        defaultGravityScale = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        desiredJump |= input.RetriveJumpInput();
        desiredJumpUp |= input.RetriveJumpInputUp();
        direction.x = input.RetriveMoveInput();
    }

    private void FixedUpdate()
    {
        onGround = ground.GetOnGround();
        velocity = new Vector2(body.velocity.x,body.velocity.y);

        if (onGround)
        {
            jumpPhase = 0;
            coyoteTimeCounter = coyoteTime;
        }
        else
        {
            coyoteTimeCounter -= Time.deltaTime;
        }
        if (desiredJump)
        {
            jumpBufferCounter = jumpBufferTime;
        }
        else
        {
            jumpBufferCounter -= Time.deltaTime;
        }
        if (desiredJump)
        {
            JumpAction();
            desiredJump = false;
        }
        if (desiredJumpUp)
        {
            coyoteTimeCounter = 0f;
            desiredJumpUp = false;
        }
        if (body.velocity.y > 0)
        {
            body.gravityScale = upwardMovementMultiplier;
        }
        else if (body.velocity.y < 0)
        {
            body.gravityScale = downwardMovementMultiplier;
        }
        else if (body.velocity.y == 0)
        {
            body.gravityScale = defaultGravityScale;
        }
        body.velocity = new Vector2(body.velocity.x, velocity.y);
    }
    private void JumpAction()
    {
        if (coyoteTimeCounter > 0f && jumpBufferCounter > 0 || jumpPhase < maxAirJumps)
        {
            jumpPhase += 1;
            float jumpSpeed = Mathf.Sqrt(-2f * Physics2D.gravity.y * jumpHeight);
            if (velocity.y > 0f)
            {
                jumpSpeed = Mathf.Max(jumpSpeed - velocity.y, 0f);
            }
            else if (velocity.y < 0f)
            {
                jumpSpeed += Mathf.Abs(body.velocity.y);
            }       
            velocity.y += jumpSpeed;
            jumpBufferCounter = 0f;
        }
    }
    public void AddAirJump(int jumpValue)
    {
        maxAirJumps = maxAirJumps + jumpValue;
    }
    public void RemoveAirJump(int jumpValue)
    {
        maxAirJumps = maxAirJumps - jumpValue;
    }
}
