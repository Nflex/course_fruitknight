using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    [SerializeField] internal InputController input = null;
    [SerializeField] private PhysicsMaterial2D noFriction;
    [SerializeField] private PhysicsMaterial2D fullFriction;
    [SerializeField, Range(0f, 100f)] private float MaxWalkSpeed = 4f;
    [SerializeField, Range(0f, 5f)] private float DragConstant = 0.9f;
    [SerializeField, Range(0f, 100f)] private float MaxAcceleration = 35f;
    [SerializeField, Range(0f, 100f)] private float MaxAirAcceleration = 20f;


    public Vector2 direction;
    public Vector2 desiredVelocity;
    public Vector2 velocity;
    public Rigidbody2D body;
    public Ground ground;

    private float maxSpeedChange;
    private float acceleration;
    internal bool onGround;

    void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        ground = GetComponent<Ground>();
    }

    void FixedUpdate()
    {
        //body.velocity = new Vector2(direction.x * MaxWalkSpeed * Time.deltaTime, body.velocity.y);
        PlayerMovement();
        PhysModif();
    }
    // Update is called once per frame
    void Update()
    {
        onGround = ground.GetOnGround();
        direction.x = input.RetriveMoveInput();
        direction.y = input.RetriveVerticalInput();
        desiredVelocity = new Vector2(direction.x, 0f) * Mathf.Max(MaxWalkSpeed - ground.GetFriction(), 0);
    }


    void PlayerMovement()
    {
        onGround = ground.GetOnGround();
        velocity = body.velocity;

        acceleration = onGround ? MaxAcceleration : MaxAirAcceleration;
        maxSpeedChange = acceleration * Time.deltaTime;
        velocity.x = Mathf.MoveTowards(velocity.x, desiredVelocity.x, maxSpeedChange);

        body.velocity = new Vector2(velocity.x, body.velocity.y);
    }
    void PhysModif()
    {
        bool directionChange = (direction.x > 0 && body.velocity.x < 0) || (direction.x < 0 && body.velocity.x > 0);
        if (Mathf.Abs(body.velocity.x) < 0.4f || directionChange)
            ApplyHorizontalDrag();
        else
            body.drag = 0;
    }
    void ApplyHorizontalDrag()
    {
        body.velocity = new Vector2(body.velocity.x * Mathf.Clamp01(1 - DragConstant), body.velocity.y);
    }
}
