using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    [SerializeField] private InputController input = null;

    public Rigidbody2D body;
    public Ground ground;
    public Animator animator;
    public Vector2 direction;
    public Vector2 velocity;

    public const string PLAYER_IDLE = "IdleAnimation";
    public const string PLAYER_WALK = "WalkAnimation";
    public const string PLAYER_RUN = "RunAnimation";
    public const string PLAYER_TURN = "TurningAnimation";
    public const string PLAYER_JUMP = "JumpAnimation";
    public const string PLAYER_FALL = "FallAnimation";

    private string currentAnimaton;
    private bool onGround;
    private bool onStickyGround;
    private bool desiredJump;
    private bool desiredJumpUp;
    protected bool facingRight = false;

    // Start is called before the first frame update
    void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        ground = GetComponent<Ground>();
    }
    void Update()
    {
        onGround = ground.GetOnGround();
        onStickyGround = ground.GetOnStickyGround();
        direction.x = input.RetriveMoveInput();
        direction.y = input.RetriveVerticalInput();

        desiredJump |= input.RetriveJumpInput();
        desiredJumpUp |= input.RetriveJumpInputUp();
    }
    private void FixedUpdate()
    {
        JumpAnimations();
        MoveAnimations();
    }
    void JumpAnimations()
    {
        if (desiredJump)
        {
            desiredJump = false;
            ChangeAnimationState(PLAYER_JUMP);
        }
        else if (body.velocity.y < -1.5)
        {
            ChangeAnimationState(PLAYER_FALL);
        }
    }
    void MoveAnimations()
    {
        if (direction.x < 0 && !facingRight || direction.x > 0 && facingRight)
            Flip();
        if (onGround)
        {
            if (body.velocity.x < 2 && body.velocity.x > -2 && direction.x != 0)
                ChangeAnimationState(PLAYER_WALK);
            else if (body.velocity.x == 2 || body.velocity.x == -2)
                ChangeAnimationState(PLAYER_WALK);
            else if (body.velocity.x > 2 || body.velocity.x < -2)
                ChangeAnimationState(PLAYER_RUN);
            else
                ChangeAnimationState(PLAYER_IDLE);
        }
        else if (onStickyGround)
        {
            if (body.velocity.x < 2 && body.velocity.x > -2 && direction.x != 0)
                ChangeAnimationState(PLAYER_WALK);
            else if (body.velocity.x == 2 || body.velocity.x == -2)
                ChangeAnimationState(PLAYER_WALK);
            else if (body.velocity.x > 2 || body.velocity.x < -2)
                ChangeAnimationState(PLAYER_WALK);
            else
                ChangeAnimationState(PLAYER_IDLE);
        }
    }
    void Flip()
    {
        ChangeAnimationState(PLAYER_TURN);
        transform.rotation = Quaternion.Euler(0, facingRight ? 0 : 180, 0);
        facingRight = !facingRight;
    }

    // mini animation manager
    void ChangeAnimationState(string newAnimation)
    {
        if (currentAnimaton == newAnimation) return;

        animator.Play(newAnimation);
        currentAnimaton = newAnimation;
    }
}
