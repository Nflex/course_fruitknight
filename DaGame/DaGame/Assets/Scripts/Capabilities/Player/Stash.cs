using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Stash : MonoBehaviour
{

    public event EventHandler OnKeysChanged;

    public TextMeshProUGUI berryCount;
    public TextMeshProUGUI orangeCount;
    public Image eggImage;
    public Image jumpImage;

    public GameObject[] players;
    private List<Key.KeyType> keylist;

    public static int berrys = 0;
    public static int oranges = 0;
    public static int eggs = 0;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }
    private void OnLevelWasLoaded(int level)
    {
        FindStartPos();
        players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length > 1){
            Destroy(players[1]);
        }
    }

    private void Awake()
    {
        jumpImage.enabled = false;
        eggImage.enabled = false;
        keylist = new List<Key.KeyType>();
    }
    
    public List<Key.KeyType> GetKeyList()
    {
        return keylist;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Key key = collision.GetComponent<Key>();

        if (key != null){
            AddKey(key.GetKeyType());
            key.gameObject.SetActive(false);
        }
        KeyDoor keyDoor = collision.GetComponent<KeyDoor>();
        if (keyDoor != null)
        {
            if (ContainesKey(keyDoor.GetKeyType()))
            {
                RemoveKey(keyDoor.GetKeyType());
                keyDoor.DoorOpen();
            }
;
        }

        if (collision.CompareTag("Collectible")){
            Collect(collision.GetComponent<Collectibles>());
        }
        void Collect(Collectibles collectible)
        {
            if (collectible.Collect())
            {
                if (collectible is BerryCollect)
                {
                    berrys++;
                }
                if (collectible is OrangeCollect)
                {
                    oranges++;
                }
                if (collectible is EggCollectible)
                {
                    eggImage.enabled = true;
                    eggs++;
                }
                UpdateGUI();
            }
        }
    }
    public void UpdateGUI()
    {

        berryCount.text = berrys.ToString();
        orangeCount.text = oranges.ToString();
    }
    public void UpdateBerrys()
    {
        berrys = 0;
        oranges = 0;
        eggs = 0;
    }
    public void AddKey(Key.KeyType keyType)
    {
        Debug.Log("Added Key: " + keyType); 
        keylist.Add(keyType);
        OnKeysChanged?.Invoke(this, EventArgs.Empty);
    }
    public void RemoveKey(Key.KeyType keyType)
    {
        keylist.Remove(keyType);
        OnKeysChanged?.Invoke(this, EventArgs.Empty);
    }
    public bool ContainesKey(Key.KeyType keyType)
    {
        return keylist.Contains(keyType);
    }
    public void FindStartPos()
    {
      transform.position = GameObject.FindWithTag("StartPos").transform.position;
    }
} 
