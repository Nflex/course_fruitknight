using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JumpUp : MonoBehaviour
{
    [SerializeField] public int jumpValue;
    private Stash stash;
    public static int jumpValueStatic;
    

    private void Awake()
    {
        stash = FindObjectOfType<Stash>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.GetComponent<Jump>().AddAirJump(jumpValue);
            jumpValueStatic = jumpValue;
            stash.jumpImage.enabled = true;
            gameObject.SetActive(false);
        }
    }
}
